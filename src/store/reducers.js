const initialState = {
  todoList: [
    {
      id: Math.random(),
      name: 'Learn React',
      completeness: false
    },
    {
      id: Math.random(),
      name: 'Learn Redux',
      completeness: false
    },
    {
      id: Math.random(),
      name: 'Learn JavaScript',
      completeness: false
    }
  ]
};

const todoList = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_TODO':
      return {
        ...state,
        todoList: state.todoList.concat({
          id: Math.random(),
          name: action.payload,
          completeness: false
        })
      };
    case 'DELETE_TODO':
      return {
        ...state,
        todoList: state.todoList.filter(item => item.id !== action.payload)
      };
    case 'CHANGE_TODO_COMPLETENESS':
      const newTodoList = [...state.todoList];
      console.log(newTodoList);
      return {
        ...state,
        todoList: newTodoList.map(item => {
          if (item.id === action.payload) {
            return {
              id: item.id,
              name: item.name,
              completeness: !item.completeness
            };
          }
          return item;
        })
      };
    default:
      return state;
  }
};

export default todoList;
