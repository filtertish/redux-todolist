export const addNewTodo = payload => {
  return {
    type: 'ADD_TODO',
    payload
  };
};

export const deleteExistingTodo = payload => {
  return {
    type: 'DELETE_TODO',
    payload
  };
};

export const changeTodoCompleteness = payload => {
  return {
    type: 'CHANGE_TODO_COMPLETENESS',
    payload
  };
};
