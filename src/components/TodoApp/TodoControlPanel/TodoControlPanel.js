import React from 'react';

import TextField from '@material-ui/core/TextField';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';

const TodoControlPanel = ({
  changeInput,
  addNewTodoToList,
  changeSortingRule,
  inputValue
}) => {
  const buttonStyle = {
    marginLeft: 20,
    marginTop: 38
  };

  const inputStyle = {
    marginTop: 30,
    width: 250
  };

  return (
    <div>
      <ButtonGroup color="primary" aria-label="outlined primary button group">
        <Button onClick={() => changeSortingRule('all')} href="">
          all
        </Button>
        <Button onClick={() => changeSortingRule('completed')} href="">
          completed
        </Button>
        <Button onClick={() => changeSortingRule('uncompleted')} href="">
          uncompleted
        </Button>
      </ButtonGroup>
      <br />
      <TextField
        id="outlined-input"
        label="Enter your todo"
        variant="outlined"
        style={inputStyle}
        onChange={changeInput}
        value={inputValue}
      />
      <Button
        href=""
        variant="outlined"
        color="primary"
        style={buttonStyle}
        onClick={addNewTodoToList}
      >
        Enter
      </Button>
    </div>
  );
};

export default TodoControlPanel;
