import React from 'react';
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';
import Item from './Item';

const TodoItems = ({
  todoList,
  deleteTodoFromList,
  changeCompleteness,
  sortingRule
}) => {
  let showingArray = [];

  if (sortingRule === 0) {
    showingArray = [...todoList];
  } else if (sortingRule === 1) {
    showingArray = [...todoList].filter(item => item.completeness === true);
  } else {
    showingArray = [...todoList].filter(item => item.completeness === false);
  }

  return (
    <ul>
      {showingArray.map(item => (
        <>
          <Item
            id={item.id}
            name={item.name}
            deleteTodoFromList={deleteTodoFromList}
            completeness={item.completeness}
          />
          <Button href="" onClick={() => changeCompleteness(item.id)}>
            {item.completeness ? 'turn back' : 'mark as completed'}
          </Button>
        </>
      ))}
    </ul>
  );
};

const mapStateToProps = state => ({ todoList: state.todoList });

export default connect(mapStateToProps)(TodoItems);
