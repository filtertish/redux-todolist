import React from 'react';
import styled from 'styled-components';

const Item = ({ name, id, completeness, deleteTodoFromList }) => {
  return (
    <TodoItem id={id} key={id} onClick={deleteTodoFromList} completeness={completeness}>
      {name}
    </TodoItem>
  );
};

const TodoItem = styled.li`
  margin-top: 40px;
  font-size: 18px;
  font-family: -apple-system, sans-serif;
  font-weight: ${props => props.completeness ? "none" : "bald"};
  text-decoration: ${props => props.completeness ? "line-through" : "none"};
  color: ${props => props.completeness ? "dimgray" : "black"};
`;

export default Item;
