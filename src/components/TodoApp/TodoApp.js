import React, { useState } from 'react';
import { connect } from 'react-redux';

import TodoControlPanel from './TodoControlPanel';
import TodoItems from './TodoItems';

import * as Actions from '../../store/actions';

const TodoApp = ({
  addNewTodo,
  deleteExistingTodo,
  changeTodoCompleteness
}) => {
  const [inputValue, updateInputValue] = useState('');
  const [sortingRule, updateSortingRule] = useState(0);

  const onMainInputChange = event => {
    updateInputValue(event.target.value);
  };

  const onEnterButton = () => {
    addNewTodo(inputValue);
    updateInputValue('');
  };

  const onLiItemClick = event => {
    deleteExistingTodo(+event.target.id);
  };

  const onCompleteButtonClick = id => {
    changeTodoCompleteness(id);
  };

  const onSortingButtonClick = prop => {
    switch (prop) {
      case 'completed':
        return updateSortingRule(1);
      case 'uncompleted':
        return updateSortingRule(2);
      default:
        return updateSortingRule(0);
    }
  };

  return (
    <>
      <TodoControlPanel
        changeInput={onMainInputChange}
        addNewTodoToList={onEnterButton}
        inputValue={inputValue}
        changeSortingRule={onSortingButtonClick}
      />
      <TodoItems
        deleteTodoFromList={onLiItemClick}
        changeCompleteness={onCompleteButtonClick}
        sortingRule={sortingRule}
      />
    </>
  );
};

const mapStateToProps = state => ({ todoList: state.todoList });

const mapDispatchToProps = dispatch => {
  return {
    addNewTodo: payload => dispatch(Actions.addNewTodo(payload)),
    deleteExistingTodo: payload =>
      dispatch(Actions.deleteExistingTodo(payload)),
    changeTodoCompleteness: payload =>
      dispatch(Actions.changeTodoCompleteness(payload))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoApp);
