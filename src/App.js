import React from 'react';

import Container from '@material-ui/core/Container';

import TodoApp from './components/TodoApp';

function App() {
  return (
    <Container maxWidth="sm">
      <h1>React/Redux todo list:</h1>
      <TodoApp />
    </Container>
  );
}

export default App;
